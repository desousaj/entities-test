### Test BACK sur les entités

L'objectif de ce test est d'implémenter un service web qui permette de gérer des entités et de pouvoir retrouver ces entités dans un texte.

Pour celà, l'exercice se décompose en 2 étapes : 

- La première étape consiste à développer la partie CRUD pour gérer les entités. 
  Il faudra donc définir les classes et les endpoints permettant de créer une nouvelle entité, la modifier ou la supprimer. 
  Le modèle de données est composé de la manière suivante : 

![Schéma](schema.png) 

Une entité est composée d'un id, un label, un type et appartient à un dictionnaire.
Un dictionnaire est composé d'un id, un nom et un secret.

Objectifs : Créer le controller avec les endpoints, créer/compléter les entités, les repositories, gérer les cas d'exceptions, faire des TUs, ...


- La 2ème étape consiste à développer un algorithme pour retrouver les entités dans un texte et de les mettre en gras.
Des tests unitaires ont été implémenté dans la classe "SearchServiceTests" pour comprendre la demande et vérifier l'algorithme.
Un endpoint a été définis dans la classe SearchController pour appeler cet algorithme. 
  Il prend en paramètre un header "secret" qui devra contenir le secret pour s'assurer que l'utilisateur a bien accès au dictionnaire.
Il prend en body, le nom d'un dictionnaire pour récupérer les entités correspondantes ainsi que le texte dans lequel il faudra rechercher les entités.
  Les résultats de ce endpoint doivent être de type SearchTextResponse avec la liste des entités trouvées et leurs positions. 
  Il doit aussi y avoir le texte initiale avec les entités entourées de balise <b>entité</b>. Pour simplifier cette partie, seul les plus petites entités doivent être entourées des balises

Objectifs : compléter l'algo pour que les TUs passent


### Test front

Créer un site web en React JS qui permette d'utiliser en partie le service back développé précédemment.
Sur ce site, il doit y avoir une page avec un champs permettant de mettre du texte dedans et un boutton pour lancer la requête.
Au clique sur ce boutton, une requête est construite pour appeler le endpoint "/api/search" dans le but de retrouver les entités.
Il faut ensuite afficher le résultat en dessous avec les entités en gras. 
Nous n'avons pas de contrainte particulière sur le design, il faut juste que la page soit ergonomique avec les résultats lisibles.

![Schéma](front.png) 
