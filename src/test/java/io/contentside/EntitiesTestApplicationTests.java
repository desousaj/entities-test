package io.contentside;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@TestPropertySource(locations="classpath:application.properties")
class EntitiesTestApplicationTests {

    @Test
    void contextLoads() {
    }

}
