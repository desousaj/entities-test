package io.contentside.service;


import io.contentside.core.enums.EntityType;
import io.contentside.dtos.EntityPositionDto;
import io.contentside.dtos.SearchTextResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@Sql(value = {"/sql/schema.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "/sql/clean-up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class SearchServiceTests {

    @Autowired
    private SearchService service;

    private static final String SECRET = "4e23376d-ee9d-451f-b7ac-9ff6d1b23b72";
    private static final String DICTIONNARY_NAME = "dictionnary_1";

    @Test
    public void test_extract_one_entity() throws Exception {
        SearchTextResponse searchResult = service.search(SECRET, DICTIONNARY_NAME, "Paris");
        Assertions.assertNotNull(searchResult);
        EntityPositionDto firstEntity = searchResult.getEntities().stream().findFirst().orElseThrow(() -> new Exception("Must not null"));
        Assertions.assertEquals("Paris", firstEntity.getEntity());
        Assertions.assertEquals(0, firstEntity.getStart());
        Assertions.assertEquals(5, firstEntity.getEnd());
    }

    @Test
    public void test_extract_multiple_entities() throws Exception {
        SearchTextResponse searchResult = service.search(SECRET, DICTIONNARY_NAME, "Paris Hilton ressemble à Paris");
        Assertions.assertNotNull(searchResult);
        List<EntityPositionDto> parisEntities = searchResult.getEntities().stream().filter(x -> x.getEntity().equals("Paris")).collect(Collectors.toList());
        Assertions.assertEquals("Paris", parisEntities.get(0).getEntity());
        Assertions.assertEquals(EntityType.LOC.name(), parisEntities.get(0).getType());
        Assertions.assertEquals(0, parisEntities.get(0).getStart());
        Assertions.assertEquals(5, parisEntities.get(0).getEnd());

        Assertions.assertEquals("Paris", parisEntities.get(1).getEntity());
        Assertions.assertEquals(EntityType.LOC.name(), parisEntities.get(1).getType());
        Assertions.assertEquals(25, parisEntities.get(1).getStart());
        Assertions.assertEquals(30, parisEntities.get(1).getEnd());

        EntityPositionDto parisHiltonEntity = searchResult.getEntities().stream().filter(x -> x.getEntity().equals("Paris Hilton")).findFirst().orElseThrow(() ->
                new Exception("Must not be null"));
        Assertions.assertEquals(EntityType.PER.name(), parisHiltonEntity.getType());
        Assertions.assertEquals(0, parisHiltonEntity.getStart());
        Assertions.assertEquals(12, parisHiltonEntity.getEnd());
    }

    @Test
    public void test_extract_without_entity() {
        SearchTextResponse searchResult = service.search(SECRET, DICTIONNARY_NAME, "John Doe");
        Assertions.assertNotNull(searchResult);
        Assertions.assertTrue(searchResult.getEntities().isEmpty());
    }

    @Test
    public void test_replace_one_entity_with_b() {
        SearchTextResponse searchResult = service.search(SECRET, DICTIONNARY_NAME, "Paris");
        Assertions.assertNotNull(searchResult);
        Assertions.assertEquals("<b>Paris</b>", searchResult.getHtml());
    }

    @Test
    public void test_replace_all_entities_with_b() {
        SearchTextResponse searchResult = service.search(SECRET, DICTIONNARY_NAME, "Paris ressemble à Paris");
        Assertions.assertNotNull(searchResult);
        Assertions.assertEquals("<b>Paris</b> ressemble à <b>Paris</b>", searchResult.getHtml());
    }

    @Test
    public void test_chevauchement_replace_only_with_the_smallest_entity() {
        SearchTextResponse searchResult = service.search(SECRET, DICTIONNARY_NAME, "Paris Hilton ne ressemble pas à Jean Paris ni à un Parisien");
        Assertions.assertNotNull(searchResult);
        Assertions.assertEquals("<b>Paris</b> Hilton ne ressemble pas à Jean <b>Paris</b> ni à un <b>Paris</b>ien", searchResult.getHtml());
    }

    @Test
    public void secret_is_not_valide() {
        //TODO Compléter le test
        Assertions.fail();
    }
}
