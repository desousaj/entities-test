CREATE TABLE dictionnary
(
    id     IDENTITY NOT NULL PRIMARY KEY,
    name   VARCHAR(50)  NOT NULL,
    secret VARCHAR(250) NOT NULL
);
CREATE TABLE entity
(
    id             IDENTITY NOT NULL PRIMARY KEY,
    label          VARCHAR(250) NOT NULL,
    type           VARCHAR(15)  NOT NULL,
    dictionnary_id INTEGER,
    FOREIGN KEY (dictionnary_id) REFERENCES dictionnary (id)
);

insert into dictionnary values ( 1, 'dictionnary_1', '4e23376d-ee9d-451f-b7ac-9ff6d1b23b72' );
insert into entity values ( 1, 'Paris Hilton', 'PER', 1);
insert into entity values ( 2, 'Paris', 'LOC', 1);
insert into entity values ( 3, 'Jean Paris', 'PER', 1);
insert into entity values ( 4, 'Parisien', 'PER', 1);
