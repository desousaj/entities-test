package io.contentside.dtos;

import lombok.Data;

import java.util.List;

@Data
public class SearchTextRequest {
    private String text;
    private String dictionnaryName;
}
