package io.contentside.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class SearchTextResponse {
    private List<EntityPositionDto> entities;
    private String html;
}
