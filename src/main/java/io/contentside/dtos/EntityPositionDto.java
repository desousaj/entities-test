package io.contentside.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EntityPositionDto {
    private int start;
    private int end;
    private String entity;
    private String type;
}
