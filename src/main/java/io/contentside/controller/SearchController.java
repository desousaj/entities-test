package io.contentside.controller;

import io.contentside.dtos.SearchTextRequest;
import io.contentside.dtos.SearchTextResponse;
import io.contentside.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/search")
public class SearchController {

    @Autowired
    private SearchService searchService;


    @PostMapping(path = "",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public SearchTextResponse search(@RequestHeader("secret") String secret, @RequestBody SearchTextRequest request) {
        return searchService.search(secret, request.getDictionnaryName(), request.getText());
    }
}
