package io.contentside.core;

import io.contentside.core.enums.EntityType;
import lombok.Data;

import javax.persistence.*;

@Data
@javax.persistence.Entity(name = "entity")
public class Entity {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String label;

    @Column
    @Enumerated(EnumType.STRING)
    private EntityType type;
}
