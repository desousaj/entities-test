package io.contentside.core;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity(name = "dictionnary")
public class Dictionnary {

    @Id
    @GeneratedValue
    private Long id;
}
