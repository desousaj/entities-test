package io.contentside;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntitiesTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(EntitiesTestApplication.class, args);
    }

}
