package io.contentside.service;

import io.contentside.dtos.EntityPositionDto;
import io.contentside.dtos.SearchTextResponse;
import io.contentside.repositories.DictionnaryRepository;
import io.contentside.repositories.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class SearchService {

    @Autowired
    EntityRepository entityRepository;

    @Autowired
    DictionnaryRepository dictionnaryRepository;

    public SearchTextResponse search(String secret, String dictionnaryName, String text) {
        //TODO 1) Faire l'algorithme qui permet de trouver les entités avec leur position dans le texte en paramètre.
        //TODO 2) Entourer les entités dans le texte initiale par les balises <b>entité</b> et le mettre dans html.
        // Seul la plus petite entité era entouré par les baslises (voir TU SearchServiceTests.test_chevauchement_replace_only_with_the_smallest_entity())
        // Remarques : seul les entités liées aux dictionnaires doivent apparaitre dans les résultats
        // Seul les dictionnaires qui ont le secret peuvent être utilisé
        // Pour valider cet algorithme et la demande initiale, les TUs de la classe SearchServiceTests doivent être tous passant.

        List<EntityPositionDto> positions = new ArrayList<>();

        entityRepository.findAll().forEach(x -> {
            int index = text.indexOf(x.getLabel());
            while (index >= 0) {
                positions.add(new EntityPositionDto(index, index + x.getLabel().length(), x.getLabel(), x.getType().toString()));
                index = text.indexOf(x.getLabel(), index + x.getLabel().length());
            }
        });

        Set<String> replaceLabels = positions.stream().map(EntityPositionDto::getEntity)
                .filter(
                        entity -> positions.stream().map(EntityPositionDto::getEntity).filter(p -> !p.equals(entity)).noneMatch(entity::contains))
                .collect(Collectors.toSet());

        String html = replaceLabels.stream().sorted(Comparator.reverseOrder())
                .map(label -> (Function<String, String>) s -> s.replaceAll(label, "<b>" + label + "</b>"))
                .reduce(Function.identity(), Function::andThen)
                .apply(text);

        //return new SearchTextResponse(Collections.emptyList(), "");
        return new SearchTextResponse(positions, html);

    }

    //Vérifier la gestion des exceptions
    //Vérifier que la comparaison avec le secret se fait directement avec la BDD
    //Voir s'il y a des tentatives d'opti de l'algo
    //Voir s'il a pensé aux chevauchements

    //Voir s'il a respecté la convention REST
    // Voir s'il a géré les cas d'exceptions (n'existe pas, ...)
}
