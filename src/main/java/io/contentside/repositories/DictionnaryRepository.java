package io.contentside.repositories;

import io.contentside.core.Dictionnary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DictionnaryRepository extends CrudRepository<Dictionnary, Long> {

}
